﻿using Aspose.Cells;
using Aspose.Cells.Drawing;
using MiniExcelLibs;
using System;
using System.Collections.Generic;
using System.IO;

/***********
 * Aspose.Cells 8.3.0
 *  
 * 
 * *********************************/

/// <summary>
/// 
/// </summary>
namespace AsposeConsole
{
    public class Program
    {
        public const string Key =
           "PExpY2Vuc2U+DQogIDxEYXRhPg0KICAgIDxMaWNlbnNlZFRvPkFzcG9zZSBTY290bGFuZCB" +
           "UZWFtPC9MaWNlbnNlZFRvPg0KICAgIDxFbWFpbFRvPmJpbGx5Lmx1bmRpZUBhc3Bvc2UuY2" +
           "9tPC9FbWFpbFRvPg0KICAgIDxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlV" +
           "HlwZT4NCiAgICA8TGljZW5zZU5vdGU+TGltaXRlZCB0byAxIGRldmVsb3BlciwgdW5saW1p" +
           "dGVkIHBoeXNpY2FsIGxvY2F0aW9uczwvTGljZW5zZU5vdGU+DQogICAgPE9yZGVySUQ+MTQ" +
           "wNDA4MDUyMzI0PC9PcmRlcklEPg0KICAgIDxVc2VySUQ+OTQyMzY8L1VzZXJJRD4NCiAgIC" +
           "A8T0VNPlRoaXMgaXMgYSByZWRpc3RyaWJ1dGFibGUgbGljZW5zZTwvT0VNPg0KICAgIDxQc" +
           "m9kdWN0cz4NCiAgICAgIDxQcm9kdWN0PkFzcG9zZS5Ub3RhbCBmb3IgLk5FVDwvUHJvZHVj" +
           "dD4NCiAgICA8L1Byb2R1Y3RzPg0KICAgIDxFZGl0aW9uVHlwZT5FbnRlcnByaXNlPC9FZGl" +
           "0aW9uVHlwZT4NCiAgICA8U2VyaWFsTnVtYmVyPjlhNTk1NDdjLTQxZjAtNDI4Yi1iYTcyLT" +
           "djNDM2OGYxNTFkNzwvU2VyaWFsTnVtYmVyPg0KICAgIDxTdWJzY3JpcHRpb25FeHBpcnk+M" +
           "jAxNTEyMzE8L1N1YnNjcmlwdGlvbkV4cGlyeT4NCiAgICA8TGljZW5zZVZlcnNpb24+My4w" +
           "PC9MaWNlbnNlVmVyc2lvbj4NCiAgICA8TGljZW5zZUluc3RydWN0aW9ucz5odHRwOi8vd3d" +
           "3LmFzcG9zZS5jb20vY29ycG9yYXRlL3B1cmNoYXNlL2xpY2Vuc2UtaW5zdHJ1Y3Rpb25zLm" +
           "FzcHg8L0xpY2Vuc2VJbnN0cnVjdGlvbnM+DQogIDwvRGF0YT4NCiAgPFNpZ25hdHVyZT5GT" +
           "zNQSHNibGdEdDhGNTlzTVQxbDFhbXlpOXFrMlY2RThkUWtJUDdMZFRKU3hEaWJORUZ1MXpP" +
           "aW5RYnFGZkt2L3J1dHR2Y3hvUk9rYzF0VWUwRHRPNmNQMVpmNkowVmVtZ1NZOGkvTFpFQ1R" +
           "Hc3pScUpWUVJaME1vVm5CaHVQQUprNWVsaTdmaFZjRjhoV2QzRTRYUTNMemZtSkN1YWoyTk" +
           "V0ZVJpNUhyZmc9PC9TaWduYXR1cmU+DQo8L0xpY2Vuc2U+";
        public static void Main(string[] args)
        {
            var root = AppDomain.CurrentDomain.BaseDirectory;
            //var template = @"D:\log\MiniTest.xlsx";
            //var path = @"D:\log\aspose_generate.xlsx";
            //var pdfPath = @"D:\log\aspose_generate.pdf";

            var template = root + @"\Template.xlsx";
            var path = root + @"\aspose_generate.xlsx";
            var pdfPath = root + @"\aspose_generate.pdf";

            var people = new people
            {
                title = "FooCompany FooCompany FooCompany FooCompany",
                managers = new List<Manager> { },
                employees = new List<employee> { }
            };
            for (int i = 0; i < 1000; i++)
            {
                people.managers.Add(new Manager { name = "Jack" + i, department = "HR" });
                people.employees.Add(new employee { name = "Wade" + i, department = "HR" });
            };

            //根据模版生成数据;
            MiniExcel.SaveAsByTemplate(path, template, people);
            //添加水印数据; 最后一页带有warning 警告;
            AddWaterMark(path);

            //保留第一页sheet, 删除 带有warning 警告的sheet 和其他sheet;
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(path);
            var names = MiniExcel.GetSheetNames(path);
            for (int i = names.Count - 1; i >= 0; i--)
            {
                var name = names[i];
                if (name != "Sheet1")
                {
                    workbook.Worksheets.Remove(name);
                }
            }
            workbook.Save();
            //Excel转换为PDF
            ExcelToPdfByAspose(path, pdfPath);
        }


        public static void ExcelToPdfByAspose(string sourcePath, string outputPath)
        {
            try
            {

                new Aspose.Cells.License().SetLicense(new MemoryStream(Convert.FromBase64String(Key)));
                Aspose.Cells.Workbook excel = new Workbook(sourcePath);
                excel.Save(outputPath, Aspose.Cells.SaveFormat.Pdf);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        /// <summary>
        /// 添加演示版水印到excel文件
        /// </summary>
        /// <param name="sourceExcelFile"></param>
        public static void AddWaterMark(string sourceExcelFile)
        {

            //string content = Cal2.Wpf.ModuleBase.ACalProductInfo.Company == ModuleBase.CompanyName.Additel ? "Demo" : "演示版";
            string content = "***演示";
            try
            {
                Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook(sourceExcelFile);
                for (int i = 0; i < workbook.Worksheets.Count; i++)
                {

                    int firstRowIndex = 0;
                    int rowMaxIndex = workbook.Worksheets[i].Cells.MaxDataRow;

                    int row = firstRowIndex + 1;
                    while (row < rowMaxIndex + firstRowIndex - 10)
                    {

                        AddWaterMarkInSheet(workbook.Worksheets[i], MsoPresetTextEffect.TextEffect2,
    content, "", 50, false, true
    , row, 0, 1, 0, 100, 500);
                        row += 30;
                    }
                }

                workbook.Save(sourceExcelFile);

            }
            catch (Exception ex)
            {

            }

        }


        /// <summary>
        /// 在sheet中添加水印
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="effect"></param>
        /// <param name="text"></param>
        /// <param name="fontName"></param>
        /// <param name="size"></param>
        /// <param name="fontBold"></param>
        /// <param name="fontItalic"></param>
        /// <param name="upperLeftRow"></param>
        /// <param name="top"></param>
        /// <param name="upperLeftColumn"></param>
        /// <param name="left"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        static void AddWaterMarkInSheet(Worksheet worksheet, MsoPresetTextEffect effect, string text,
            string fontName, int size, bool fontBold, bool fontItalic, int upperLeftRow,
            int top, int upperLeftColumn, int left, int height, int width)
        {
            Aspose.Cells.Drawing.Shape wordart = worksheet.Shapes.AddTextEffect(
                effect, text, fontName, size, fontBold, fontItalic, upperLeftRow, top, upperLeftColumn, left, height, width);

            MsoFillFormat wordArtFormat = wordart.FillFormat;
            wordArtFormat.ForeColor = System.Drawing.Color.Red;
            wordArtFormat.Transparency = 0.9;
            MsoLineFormat lineFormat = wordart.LineFormat;
            lineFormat.IsVisible = false;
            wordart.SetLockedProperty(ShapeLockType.Selection, true);
            wordart.SetLockedProperty(ShapeLockType.ShapeType, true);
            wordart.SetLockedProperty(ShapeLockType.Move, true);
            wordart.SetLockedProperty(ShapeLockType.Resize, true);
            wordart.SetLockedProperty(ShapeLockType.Text, true);
        }
    }

    public class people
    {
        public string title { get; set; }
        public List<Manager> managers { get; set; }
        public List<employee> employees { get; set; }


    }

    public class Manager
    {
        public string name { get; set; }
        public string department { get; set; }
    }

    public class employee
    {
        public string name { get; set; }
        public string department { get; set; }
    }
}
